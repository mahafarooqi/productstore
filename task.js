/**
 * Created by studentwtep on 7/1/2017.
 */
var myyApp=angular.module('myyApp', []);
myyApp.controller('appCtrl', function ($scope) {
    $scope.products = [
        {name: 'Malt Cake', weight: 3, quan: 4, imagepath:'malt.jpg'},
        {name: 'Oreo Cake', weight: 2, quan: 5, imagepath:'oreo.jpg'},
    ];
    $scope.new = {};
    $scope.addProduct=function () {
        $scope.new.imagepath='malt.jpg'
        $scope.products.push($scope.new)
    }
});
